# PHP Telepathy API Wrapper 👻

### [Official documentation](http://telepathy-api.ap-southeast-2.elasticbeanstalk.com/docs)

## Configuration
The library should be configured with an api token before it can be used.

```php
HttpClient::init('API-TOKEN-HERE');
```

Once configured, using the library is simple. Just call the methods from the available endpoints.

## Quick Start

### Send an SMS Campaign ✉️

```php
use Distillery\Telepathy\SMS;

// Campaign Name, SMS Text, Array of recipients
SMS::launch("My Campaign", "Wooho, you have won something!", [
    'first_name': 'John',
    'last_name': 'Doe',
    'destination': '0400000000',
]);
```

### Send a Phonemail Campaign 📲

⚠️ : You must already have the recording tied to your account in Telepathy. 

```php
use Distillery\Telepathy\Phonemail;

// Campaign Name, Recording id, Array of recipients
Phonemail::launch("My Campaign", "1", [
    'first_name': 'John',
    'last_name': 'Doe',
    'destination': '0400000000',
]);
```

----

## Todo
1. .ENV based api token [✅]
2. Tests
