<?php

namespace Distillery\Telepathy;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\CurlHandler;

/**
 * The HttpClient class
 */
class HttpClient
{
    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @var string
     */
    private static $host = 'telepathy-api.ap-southeast-2.elasticbeanstalk.com';

    /**
     * @var string
     */
    private $endpoint = '/api/v1/';
    
    /**
     * @var string
     */
    private $scheme = 'http';

    /**
     * @var string|null The Telepathy API authorization token
     */
    private static $token = null;

    public function __construct()
    {
        if (self::$token === null) {
            throw new \Exception("\nAn API token is required. Please initialize the library with a token first.\n\n");
        }

        $this->httpClient = new Client([
            'base_uri' => self::$host . $this->endpoint,
            'headers' => 
                [
                    'X-AUTH-TOKEN' => self::$token,
                ],
        ]);
    }

    /**
     * Initialize the auth token required across all api endpoints
     */
    public static function init(string $token)
    {
        self::$token = $token;
    }

    public static function setHost(string $host)
    {
        self::$host = $host;
    }

    /**
     * Send an http request through the http client.
     *
     * @param  string $method http method sent
     * @param  array $arguments Array containing the URI to send the request and the parameters of the request
     * @return \GuzzleHttp\Psr7\Response
     */
    public function send($method, $arguments)
    {
        $uri = $arguments[0];
        $params = isset($arguments[1]) ? $arguments[1] : [];

        $response = $this->httpClient->send(
            new Request($method, new Uri($uri)),
            $params
        );

        return $response;
    }

    /**
     * Generate a new handler that will manage the HTTP requests.
     *
     * Middleware configred to manage the request URI
     *
     * @param string $authorization Authorization code to pass in the header
     * @return \GuzzleHttp\HandlerStack
     */
    private function setHandler()
    {
        $stack = new HandlerStack();
        $stack->setHandler(new CurlHandler());

        $stack->push(Middleware::mapRequest(function (Request $request) {
            $uri = $request->getUri()->withScheme($this->scheme);

            return $request->withUri($uri);
        }), 'set_host');

        return $stack;
    }

    public static function test()
    {
        return 'Telepathy SDK is present';
    }
}
