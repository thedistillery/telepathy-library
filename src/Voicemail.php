<?php

namespace Distillery\Telepathy;

/**
 * The Voicemail Class
 */
class Voicemail extends Endpoint
{
    use Launcher;

    const NAMESPACE = 'send_voicemail_request';
    const ENDPOINT = 'voicemails';
    const MESSAGE_PROP = 'recording';

    /**
     * Get all voicemails.
     * 
     * @return array
     */
    public static function all()
    {
        return self::onResponse(
            self::get("voicemails")
        );
    }

    public static function find(int $id)
    {
        return 'Unavailable';
    }

    public function start()
    {
        return 'Unavailable';

        return self::onResponse(
            self::patch("voicemails/{$this->id}/start")
        );
    }

    public function stop()
    {
        return 'Unavailable';

        return self::onResponse(
            self::patch("voicemails/{$this->id}/stop")
        );
    }

    public function delete()
    {
        return 'Unavailable';

        return self::onResponse(
            self::delete("voicemails/{$this->id}")
        );
    }
}
