<?php

namespace Distillery\Telepathy;

/**
 * The FormData class
 */
class FormData
{
    /**
     * @var string
     */
    private $namespace;

    /**
     * @var array
     */
    private $data;

    /**
     * Constructs a new namespaced form data object
     * @param string $namespace
     * @param array $recipients
     * @throws Exception
     */
    public function __construct(string $namespace, array $recipients)
    {
        $this->namespace = $namespace;

        foreach ($recipients as $key => $recipient) {
            $this->addRecipient($recipient, $key);
        }
    }

    /**
     * Add a recipient to the form.
     *
     * @param array $recipient
     * @param int $index
     * @return void
     * @throws Exception
     */
    public function addRecipient(array $recipient, int $index)
    {
        if (array_key_exists('destination', $recipient)) {
            $this->data["{$this->namespace}[recipients][{$index}]"] = $recipient;
        } else {
            throw new Exception("Destination value is missing for recipient");
        }
    }

    /**
     * Append a data to the form.
     *
     * @return FormData
     */
    public function append(string $key, string $value)
    {
        $key = "{$this->namespace}[{$key}]";
        $this->data[$key] = $value;
        return $this;
    }

    /**
     * Get the form data.
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }
}