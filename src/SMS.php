<?php

namespace Distillery\Telepathy;

/**
 * The SMS Class
 */
class SMS extends Endpoint
{
    use Launcher;

    const NAMESPACE = 'send_sms_request';
    const ENDPOINT = 'sms';
    const MESSAGE_PROP = 'sms_text';

    /**
     * Get the report of an SMS campaign.
     * 
     * @return array
     */
    public function report()
    {
        return self::onResponse(
            self::get("sms/{$this->id}")
        );
    }

    /**
     * Get the id of the campaign.
     * 
     * @return int
     */
    public function id()
    {
        return $this->id;
    }
}