<?php

namespace Distillery\Telepathy;

class ContactList extends Endpoint
{
    use ResponseHandler;

    /**
     * Retrieve a contact list by id.
     * 
     * @return ContactList
     */
    public static function find($id)
    {
        return self::handleResponse(
            self::get("lists/{$id}")
        );
    }

    /**
     * Retrieve all contact lists.
     * 
     * @return array
     */
    public static function all()
    {
        return self::onResponse(
            self::get('lists')
        );
    }

    /**
     * Create a new contact list.
     * 
     * @todo implement
     */
    public static function create()
    {
        return 'Unavailable';
    }

    /**
     * Delete a contact list.
     * 
     * @return boolean
     */
    public function remove()
    {
        self::delete("lists/{$this->id}");
        return true;
    }
}