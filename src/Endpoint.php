<?php

namespace Distillery\Telepathy;

use GuzzleHttp\Psr7\Response;

/**
 * The Endpoint Class
 */
class Endpoint
{
    const ACCESS_DENIED = 'detail unavailable';

    /**
     * @var array - Endpoint's parameters
     */
    private $parameters;

    /**
     * @var array - Currently accepted methods
     */
    private static $acceptedHttpMethods = ['get', 'post', 'patch', 'delete'];

    /**
     * Construct a new endpoint object and set the parameters from an array.
     *
     * @param array $parameters
     */
    public function __construct($parameters = [])
    {
        $this->parameters = $parameters;
    }

    /**
     * Magic method to retrieve a specific parameter in the parameters array
     *
     * @param  string $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->parameters[$key];
    }

    /**
     * Magic method to inform if specific parameter is available
     *
     * @param  string $key
     * @return boolean
     */
    public function __isset($key)
    {
        return isset($this->parameters[$key]);
    }

    /**
     * Returns any returned data
     *
     * @return array
     */
    public function getData() {
        return $this->parameters;
    }

    /**
     * Check if the HTTP method is accepted and send a HTTP request to it.
     * Retrieve error from the request and throw a new error
     *
     * @param  string $method HTTP action to trigger
     * @param  array $arguments Array containing all the parameters pass to the magic method
     * @throws \Exception if the HTTP request failed
     * @see \Distillery\Telepathy\HttpClient::send()
     * @return Response|null
     */
    public static function __callStatic($method, $arguments)
    {
        // Validate if the $method is part of the accepted http method array
        if (!in_array($method, self::$acceptedHttpMethods)) {
            return null;
        }

        $httpClient = new HttpClient();
        $response = $httpClient->send($method, $arguments);
        
        // Validate if the request failed
        if (!self::validRequest($response)) {
            throw new \Exception($response->getBody(), $response->getStatusCode());
        }

        return $response;
    }

    /**
     * Retrieve the error messages in the body
     *
     * @param  \GuzzleHttp\Psr7\Response $response of the HTTP request
     * @return array Array of error messages
     */
    private static function getErrorMessage($response)
    {
        $body = $response->getBody();

        $data = json_decode($body, true);
        $errors = [];

        if (is_array($data) && isset($data['message'])) {
            $errors = $data;
        }

        return $errors;
    }

    /**
     * Retrieve the response status code and determine if the request was successful.
     *
     * @param  \GuzzleHttp\Psr7\Response $response of the HTTP request
     * @return boolean
     */
    public static function validRequest($response)
    {
        return $response->getstatusCode() >= 200 && $response->getstatusCode() < 300;
    }
}
