<?php

namespace Distillery\Telepathy;

/**
 * The Recording class
 */
class Recording extends Endpoint
{
    use ResponseHandler;

    const PATH = "recordings";

    /**
     * Get all recordings tied to account from Telepathy.
     *
     * @return array
     */
    public static function all()
    {
        return self::onResponse(
            self::get(static::PATH)
        );
    }

    /**
     * Uploads a new voice recording (must be a .wav file)
     *
     * @param string $name
     * @param string $data
     * @return array
     */
    public static function upload($name, $data)
    {

        $payload = [
            'name' => $name,
            'file' => $data
        ];

        return self::onResponse(
            self::post(static::PATH, ['upload_request' => $payload])
        );
    }

    /**
     * Deletes an existing recording
     *
     * @param $recordingId
     * @return array
     */
    public static function delete($recordingId) {
        return self::onResponse(
            self::delete(static::PATH."/".$recordingId)
        );
    }
}
