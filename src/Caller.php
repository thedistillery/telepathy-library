<?php

namespace Distillery\Telepathy;

/**
 * The Caller Class
 */
class Caller extends Endpoint
{
    use ResponseHandler;

    const PATH = "callers";

    /**
     * Find a caller.
     *
     * @param $callerId
     * @return Endpoint
     */
    public static function find($callerId)
    {
        return self::handleResponse(
            self::get(static::PATH."/".$callerId)
        );
    }

    /**
     * Returns all callers
     * @return Endpoint
     */
    public static function all() {
        return self::handleResponse(
            self::get(static::PATH)
        );
    }

    /**
     * Creates a new caller ID
     *
     * @param $name
     * @param $number
     * @param null $companyName
     * @return array
     * @throws Exception
     */
    public static function create($name, $number, $companyName = null) {

        $name = trim($name);
        if (strlen($name) === 0) {
            throw new Exception("Name must be provided");
        }

        $number = trim(str_replace(" ", "", $number));
        if (strlen($number) !== 10) {
            throw new Exception("Mobile number must be 10 characters");
        }

        $data = [
            'name' => $name,
            'phone_number' => $number,
        ];

        if (!is_null($companyName)) {
            $data['company_name'] = trim($companyName);
        }

        return self::onResponse(
            self::post(static::PATH, ['form_params' => ['create_caller_request' => $data]])
        );

    }

    /**
     * Requests that a verification code be sent to the caller's phone
     * @param $callerId
     * @return array
     */
    public static function requestVerification($callerId) {
        return self::onResponse(
            self::patch(static::PATH."/".$callerId."/verify")
        );
    }

    /**
     * Verifies the code that was sent to the caller's phone
     *
     * @param $callerId
     * @param $code
     * @return array
     */
    public static function verifyCallerId($callerId, $code) {
        return self::onResponse(
            self::patch(static::PATH."/".$callerId."/verify?code=".trim($code))
        );
    }

}
