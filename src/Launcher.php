<?php

namespace Distillery\Telepathy;

/**
 * The Launcher Trait
 */
trait Launcher
{
    use ResponseHandler;

    /**
     * Launch a campaign
     *
     * @param $callerId
     * @param string $campaignName
     * @param string $message - Has to be a string for SMS and a string number for Phonemail or Voicemail campaigns
     * @param array $recipients an array of recipients in the format:
     * [
     *  'destination' => (required) phone number/email/etc
     *  'first_name' => (optional) first name
     *  'last_name' => (optional) last name
     * ]
     *
     * @return self|null
     */
    public static function launch(int $callerId, string $campaignName, string $message, array $recipients)
    {
        try {
            $form = new FormData(self::NAMESPACE, $recipients);

            $form
                ->append('caller', $callerId)
                ->append('campaign_name', $campaignName)
                ->append(self::MESSAGE_PROP, $message);

            $response = self::post(self::ENDPOINT, [
                'form_params' => $form->getData(),
            ]);

            return self::handleResponse($response);
        } catch (Exception $e) {

        }
    }
}