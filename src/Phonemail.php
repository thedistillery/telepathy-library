<?php

namespace Distillery\Telepathy;

/**
 * The Phonemail Class
 */
class Phonemail extends Endpoint
{
    use Launcher;

    const NAMESPACE = 'send_voice_request';
    const ENDPOINT = 'voices';
    const MESSAGE_PROP = 'recording';

    /**
     * Get the report of a Phonemail campaign.
     * 
     * @return array
     */
    public function report()
    {
        return self::onResponse(
            self::get("voices/{$this->id}")
        );
    }

    /**
     * Get the id of the campaign.
     * 
     * @return int
     */
    public function id()
    {
        return $this->id;
    }
}