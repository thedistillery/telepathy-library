<?php

namespace Distillery\Telepathy;
use GuzzleHttp\Psr7\Response;

trait ResponseHandler
{
    /**
     * Constructs a new (Endpoint) object with the given parameters.
     * 
     * @param GuzzleHttp\Psr7\Response $response
     * @return self
     */
    private static function handleResponse(Response $response)
    {
        if (Endpoint::validRequest($response)) {
            $parameters = [];
            if ($decoded = json_decode($response->getBody(), true)) {
                // payload key missing if there is no data to return
                if (array_key_exists('payload', $decoded)) {
                    $parameters = $decoded['payload'];
                } else {
                    $parameters = $decoded;
                }
            }


            return new self($parameters);
        }

        return null;
    }

    /**
     * Transform to array on a valid response with payload.
     * 
     * @param GuzzleHttp\Psr7\Response $response
     * @return array
     */
    private static function onResponse(Response $response)
    {
        if (Endpoint::validRequest($response)) {
            $response = json_decode($response->getBody(), true);

            return (isset($response['payload'])) ? $response['payload'] : $response;
        }
    }
}