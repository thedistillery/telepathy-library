<?php
namespace Distillery\Telepathy;

require '../vendor/autoload.php';
$dotenv = \Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

HttpClient::init(getenv('API_TOKEN'));
HttpClient::setHost('localhost:81');

$recipients = [
    [
        "first_name" => "Matthew",
        "last_name" => "Torpy",
        "destination" => "0453635636"
    ],
    [
        "first_name" => "Scarlett",
        "last_name" => "Wilkie",
        "destination" => "0492588279"
    ],
    [
        "first_name" => "Alexandra",
        "last_name" => "Verran",
        "destination" => "0445058416"
    ]
];

///------------------------------------
/// Contact List
///------------------------------------
var_dump(ContactList::all());
var_dump(ContactList::find(1));

///------------------------------------
/// Recording
///------------------------------------
var_dump(Recording::all());

///------------------------------------
/// SMS
///------------------------------------
$campaign = SMS::launch("Alpha", "Lorem, ipsum dolor.", $recipients);
var_dump($campaign->report());

///------------------------------------
/// Phonemail
///------------------------------------
$campaign = Phonemail::launch("Betaa", "1", $recipients);
var_dump($campaign->report());

///------------------------------------
/// Voicemail
///------------------------------------
$campaign = Voicemail::launch("Charlie", "1", $recipients);
var_dump($campaign->start());
